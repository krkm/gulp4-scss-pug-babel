const SRC = {
    DEV: {
        ROOT: './dev',
        FONTS: './dev/static/fonts/',
        IMAGES: './dev/static/images/',
        PUG: './dev/pug/',
        SCSS: './dev/static/styles/',
        JS: './dev/static/js/'
    },
    PROD: {
        ROOT: './build',
        FONTS: './build/static/fonts/',
        IMAGES: './build/static/images/',
        CSS: './build/static/css/',
        JS: './build/static/js/'
    }
};
module.exports = function() {
    $.gulp.task('fonts', () => {
        return $.gulp
            .src(SRC.DEV.FONTS + '**/*.*')
            .pipe($.gulp.dest(SRC.PROD.FONTS));
    });
};
