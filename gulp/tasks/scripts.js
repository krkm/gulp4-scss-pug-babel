const SRC = {
    DEV: {
        ROOT: './dev',
        FONTS: './dev/static/fonts/',
        IMAGES: './dev/static/images/',
        PUG: './dev/pug/',
        SCSS: './dev/static/styles/',
        JS: './dev/static/js/'
    },
    PROD: {
        ROOT: './build',
        FONTS: './build/static/fonts/',
        IMAGES: './build/static/images/',
        CSS: './build/static/css/',
        JS: './build/static/js/'
    }
};

let babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    scriptsPATH = {
        input: SRC.DEV.JS,
        ouput: SRC.PROD.JS
    };

module.exports = function() {
    $.gulp.task('libsJS:dev', () => {
        return $.gulp
            .src(['node_modules/svg4everybody/dist/svg4everybody.min.js'])
            .pipe(concat('libs.min.js'))
            .pipe($.gulp.dest(scriptsPATH.ouput));
    });

    $.gulp.task('libsJS:build', () => {
        return $.gulp
            .src(['node_modules/svg4everybody/dist/svg4everybody.min.js'])
            .pipe(concat('libs.min.js'))
            .pipe(uglify())
            .pipe($.gulp.dest(scriptsPATH.ouput));
    });

    $.gulp.task('js:dev', () => {
        return $.gulp
            .src([
                scriptsPATH.input + '*.js',
                '!' + scriptsPATH.input + 'libs.min.js'
            ])
            .pipe(
                babel({
                    presets: ['@babel/env']
                })
            )
            .pipe($.gulp.dest(scriptsPATH.ouput))
            .pipe(
                $.browserSync.reload({
                    stream: true
                })
            );
    });

    $.gulp.task('js:build', () => {
        return $.gulp
            .src([
                scriptsPATH.input + '*.js',
                '!' + scriptsPATH.input + 'libs.min.js'
            ])
            .pipe($.gulp.dest(scriptsPATH.ouput));
    });

    $.gulp.task('js:build-min', () => {
        return $.gulp
            .src([
                scriptsPATH.input + '*.js',
                '!' + scriptsPATH.input + 'libs.min.js'
            ])
            .pipe(concat('main.min.js'))
            .pipe(uglify())
            .pipe($.gulp.dest(scriptsPATH.ouput));
    });
};
