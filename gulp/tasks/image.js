const SRC = {
    DEV: {
        ROOT: './dev',
        FONTS: './dev/static/fonts/',
        IMAGES: './dev/static/images/',
        PUG: './dev/pug/',
        SCSS: './dev/static/styles/',
        JS: './dev/static/js/'
    },
    PROD: {
        ROOT: './build',
        FONTS: './build/static/fonts/',
        IMAGES: './build/static/images/',
        CSS: './build/static/css/',
        JS: './build/static/js/'
    }
};
let imagemin = require('gulp-imagemin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    imgPATH = {
        input: [
            SRC.DEV.IMAGES + '**/*.{png,jpg,gif,svg}',
            '!' + SRC.DEV.IMAGES + 'svg/*'
        ],
        ouput: SRC.PROD.IMAGES
    };

module.exports = function() {
    $.gulp.task('img:dev', () => {
        return $.gulp.src(imgPATH.input).pipe($.gulp.dest(imgPATH.ouput));
    });

    $.gulp.task('img:build', () => {
        return $.gulp
            .src(imgPATH.input)
            .pipe(
                cache(
                    imagemin(
                        [
                            imagemin.gifsicle({ interlaced: true }),
                            imagemin.jpegtran({ progressive: true }),
                            imageminJpegRecompress({
                                loops: 5,
                                min: 70,
                                max: 75,
                                quality: 'medium'
                            }),
                            imagemin.svgo(),
                            imagemin.optipng({ optimizationLevel: 3 }),
                            pngquant({ quality: '65-70', speed: 5 })
                        ],
                        {
                            verbose: true
                        }
                    )
                )
            )
            .pipe($.gulp.dest(imgPATH.ouput));
    });
};
